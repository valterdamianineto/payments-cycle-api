const mongoose = require('mongoose')
mongoose.Promise = global.Promise

module.exports = mongoose.connect('mongodb://localhost/mymoney', { useNewUrlParser: true })


mongoose.Error.messages.general.required = "the '{PATH}' attribute is required"
mongoose.Error.messages.Number.min = "The number entered is less than the allowed limit"
mongoose.Error.messages.Number.max = "The number entered is greater than the allowed limit"