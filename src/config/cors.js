module.exports = (req, res, next) => {
    req.header('Access-Control-Allow-Origin', '*')
    req.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE, PATH')
    req.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
}